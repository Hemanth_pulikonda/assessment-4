import java.util.Scanner;
public class Question5 {
	 public static void main(String[] args) 
	  {
		Scanner s = new Scanner(System.in);
		
		System.out.println("Enter Any Name");
		
		String st = s.nextLine();
		
	    //String s = "Hello, have a good day";
	    
	    int vowels = 0, consonants = 0, digits = 0, spaces = 0;

	    st = st.toLowerCase();
	    
	    for (int i = 0; i < st.length(); ++i) 
	    {
	      char ch = st.charAt(i);

	      if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u')
	      {
	        ++vowels;
	      }

	      else if ((ch >= 'a' && ch <= 'z'))
	      {
	        ++consonants;
	      }
	      
	      else if (ch >= '0' && ch <= '9')
	      {
	        ++digits;
	      }
	      
	      else if (ch == ' ') 
	      {
	        ++spaces;
	      }
	    }

	    System.out.println("Number of Vowels : " + vowels);
	    System.out.println("Number of Consonants : " + consonants);
	    System.out.println("Number of Digits : " + digits);
	    System.out.println("Number of White spaces : " + spaces);
	  }
}
