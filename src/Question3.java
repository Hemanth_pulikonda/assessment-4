
public class Question3 {
	public static void main(String args[])
	{
		String s 		= "Hello, World";
		char search 	= 'o';
		
		int count = 0;
		for(int i=0; i<s.length(); i++)
		{
			if(s.charAt(i) == search)
				count++;
		}
		
		System.out.println("The Character " + search+" appears " +count+" times. ");
	}

}